---
title: "Community"
date: 2021-05-11
hide_sidebar: "false"
---


---
### Eclipse Automotive
---

[Project page]
(https://projects.eclipse.org/projects/automotive "Automotive - Project")

* [Who is involved]
  (https://projects.eclipse.org/projects/automotive/who "Automotive - Who")

* [Governance / Eclipse Development Process]
  (https://projects.eclipse.org/projects/automotive/governance "Automotive - Governance")

[Wiki]
(https://wiki.eclipse.org/Automotive "Automotive Wiki")

[Mailing list]
(https://accounts.eclipse.org/mailing-list/automotive-pmc "Automotive Mailing list")
