---
date: 2021-04-30
title: "APP4MC - Release 1.1.0 published"
categories: ["news"]
slug: "2021-04-30-release-1-1-0"
---

We released a new version of APP4MC with a couple of new features and improvements.

<!--more-->

__Extract of APP4MC 1.1.0__


Model handling

	Model migration support (1.0.0 -> 1.1.0)

Product

	Improved handling of models in the Amalthea Model Editor
	- model loading on demand
	- faster loading of (large) models
	- improved resolution of cross-file references

	Reduced dependencies of the Amalthea model
	(allows easier use in non Eclipse environments)

	Removed "Amalthea no-load nature"
	(no longer required because of model loading on demand)

	New visualization of scheduler mapping

	New APP4MC.sim validations

	Several bug fixes

Recommended Java runtime is **Java 11**. Minimum is Java 8 (with limitations if JavaFX is not included).


__Further details__

* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
